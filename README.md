# Déploiment de l'infrastructure du service Yitus

## TODOLIST

* nginx + let's encrypt
* docker-compose
    * app spring
    * postgres
    * postgres_backup
    * mail service

* firewall (ufw)
* fail2ban

* déploiement multi-machines (kubernetes ?)

* stress testing (load injection)
* audit sécurité
* audit RGPD
